public class Board {
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board(){
		this.die1 = new Die();
		this.die2 = new Die();
		this.tiles = new boolean[12];
	}
	
	public String toString(){
		String tileString = "";
		for(int i = 0; i <tiles.length; i++){
			if (tiles[i] == false){
				tileString += i +1 + " ";
			}
			else{
				tileString += "X ";
			}
		}
		return tileString;
	}
	
	public boolean playATurn(){
		die1.roll();
		die2.roll();
		System.out.println("die 1: " + die1);
		System.out.println("die 2: " +  die2);
		
		int sumOfDice = die1.getFaceValue() +die2.getFaceValue();
		
		if(tiles[sumOfDice - 1] == false){ 
			tiles[sumOfDice - 1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
			System.out.println("");
			return false;
		}
		
		else if(tiles[die1.getFaceValue()-1] == false){
			tiles[die1.getFaceValue()-1] = true;
			System.out.println("Closing tile with the same value as die one: " + die1.getFaceValue());
			System.out.println("");
			return false;
		}
		
		else if(tiles[die2.getFaceValue()-1] == false){
			tiles[die2.getFaceValue()-1] = true;
			System.out.println("Closing tile with the same value as die two: " + die2.getFaceValue());
			System.out.println("");
			return false;
		}
		
		else{
			System.out.println("All the tiles for these values are already shut");
			return true;
		}	
		
	}
}