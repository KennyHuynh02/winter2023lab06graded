import java.util.Random;
public class Die {
	private int faceValue;
	private Random random;
	
	public Die(){
		this.faceValue = 1;
	}
	
	public int getFaceValue(){
		return this.faceValue;
	}
	
	public void roll(){
		Random random = new Random();
		this.faceValue = random.nextInt(5) +1;
		
	}
	
	public String toString(){
		return this.faceValue +"";
	}
}